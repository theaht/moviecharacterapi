## Movie Character API 
  
### Description  
The Movie Character API application is a java Spring Web application, using the object-relational mapping tool Hibernate. The application uses a PostgreSQL database with a REST API to interact and manipulate the data in the database. 

The application consists of three model classes: Character, Movie and Franchise.

The database stores information about the characters, which movies they appear in, and what franchise the movies belongs to. 

>Characters and movies have a many-to-many relationship. Movies and franchises have a one-to-many relationship.  

## Table columns
The information stored for each entity:
 
**Character**
* Auto incremented Id
* Full Name
* Alias (if applicable)
* Gender
* Picture

**Movie**
* Auto incremented Id
* Movie title
* Genre
* Release year
* Director
* Picture
* Trailer

**Franchise**
* Auto incremented Id
* Name
* Description

The database also contains an associative table named character_movie, that contains the character id and movie id (primary keys), for the movies that the characters appear in. 

## Rest endpoints
All the entities have full CRUD to be managed. This includes READ ALL and READ ONE.
 
 There are also three custom endpoints:
* Get all the movies for a franchise
* Get all characters in a movie
* Get all the characters in a franchise

## Additional resources 
The endpoint hierarchy is documented with Swagger/Open API, and can be found at: [Swagger](https://spicegirls-movieapi.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/)

The database was created using pgAdmin. 
The tables and data can be created using the sql dump in the root directory.
The endpoints can be tested using the postman collection, which has been exported to a JSON file in the root directory.

Both the database and the application have been published on Heroku: [Heroku-app](https://spicegirls-movieapi.herokuapp.com/).
To test that the database is configured correctly on Heroku, press this [Link](https://spicegirls-movieapi.herokuapp.com/api/v1/franchise/6/characters) to see all characters in Spice girls.

![Alt Text](https://media1.giphy.com/media/TFNeJUfQR76SY5DbpB/giphy.gif?cid=ecf05e47sw71dys1hzzc88xm6kz0hrb64ctrl3xl5us5fmho&rid=giphy.gif)
---

Application made by:
Thea Thodesen,
Camilla Stokbro Felin,
Maren Ytterdal Krogsrud,
Sarah Thorstensen

---
Project repo can be found at: [GitLab](https://gitlab.com/SarahMags/moviecharacterapi)
