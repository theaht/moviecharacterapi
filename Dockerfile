FROM openjdk:15
VOLUME tmp
ADD target/MovieCharacterAPI-0.0.1-SNAPSHOT.jar MovieCharacterAPI.jar
ENTRYPOINT ["java", "-jar", "/MovieCharacterAPI.jar"]