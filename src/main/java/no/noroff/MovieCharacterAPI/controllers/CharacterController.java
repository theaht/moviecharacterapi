package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    /**
     * This getmapping returns a list of all characters in the database.
     * Together with the status code 200-OK.
     * */
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all characters",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Character.class)))}) })
    @GetMapping("/characters")
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters,status);
    }

    /**
     * This finds a specific character based on the path variable id. If the character is found,
     * it is returned together with the status code 200-OK.
     * If the character does not exists, the status code 404 - Not found is returned.
     * */
    @Operation(summary = "Get a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @GetMapping("/character/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable Long id){
        Character returnChar = new Character();
        HttpStatus status;

        if (characterRepository.existsById(id)){
            status = HttpStatus.OK;
            returnChar = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnChar, status);
    }

    /**
     * This postmapping takes in a character and adds it to the database.
     * It returns the newly added character, together with the status code 201-created.
     * */
    @Operation(summary = "Add a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }) })
    @PostMapping("/character")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character returnChar = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnChar, status);
    }

    /**
     * This updates an existing character in the database.
     * It takes in an id together with a character object with the updated information.
     * If the id in the path variable does not match the id in the request body,
     * the status code 400 - bad request is returned.
     * If the id's match the character is updated and returned together with status 204 no content.
     * */
    @Operation(summary = "Update an existing character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated character",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/character/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        Character returnChar = new Character();
        HttpStatus status;

        if(!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnChar,status);
        }
        returnChar = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnChar,status);
    }

    /**
     * This takes in an id and if the character exists,
     * it is removed from the movie(s) it is in.
     * Then the character is deleted form the database.
     * The status 200-OK is returned when the character is deleted.
     * If the character does not exists the status 404-not found is returned.
     * */
    @Operation(summary = "Delete character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Character deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @DeleteMapping("character/{id}")
    public ResponseEntity<Character> deleteCharacter(@PathVariable long id){
        HttpStatus  status;

        if (characterRepository.existsById(id)) {

            Character character = characterRepository.findById(id).get();
            for (Movie movie: character.getMovies()){
                movie.getCharacters().remove(character);
            }
            characterRepository.delete(character);
            status = HttpStatus.OK;
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(null, status);
    }


}
